from subprocess import run, PIPE
from urllib.request import urlopen
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from datetime import datetime

import argparse
import time

from selenium import webdriver
from selenium.webdriver.firefox.options import Options


def get_windy():
    opts = Options()
    opts.headless = True
    opts.add_argument("--width=1800")
    opts.add_argument("--height=1200")
    opts.add_argument("--pixelRatio=1.0")

    browser = webdriver.Firefox(options=opts)
    browser.get('https://www.windy.com/multimodel/52.78/-170.024?cbase,53.16,-168.138,8')
    time.sleep(1)
    img = browser.get_screenshot_as_png()
    browser.close()
    attachment = make_attachment(img, "windy.png")
    return attachment


def send_message(subject, body_text, attachments = []):
    recipients = ['menders@usgs.gov', 'endersml@gmail.com', 'Jordanrickyoung@gmail.com', 
                  'esboyce@alaska.edu', 'ijbrewster@alaska.edu']
    # recipients = ['israel@brewstersoft.net']
    for recipient in recipients:
        msg = MIMEMultipart()
        msg['From'] = 'Weather Bot <no-reply@apps.avo.alaska.edu>'
        msg['To'] = recipient
        msg['Date'] = formatdate(localtime = True)
        msg['Subject'] = subject
        msg.attach(MIMEText(body_text))
        for attachment in attachments:
            msg.attach(attachment)

        run(["/usr/sbin/sendmail", "-t", "-oi"], input = msg.as_bytes(),
            stdout = PIPE, stderr = PIPE)


def make_attachment(data, filename):
    part = MIMEApplication(data, Name = filename)
    part['Content-Disposition'] = f'attachment; filename="{filename}"'
    return part


if __name__ == "__main__":
    URLS = ['https://www.weather.gov/images/aawu/sigWx24.gif',
            'https://www.weather.gov/images/aawu/sigWx36.gif',
            'https://www.weather.gov/images/aawu/sigWx48.gif',
            'https://www.weather.gov/images/aawu/sigWx60.gif',
            'https://www.weather.gov/images/aawu/fcstgraphics/ifr_1.png',
            'https://www.weather.gov/images/aawu/fcstgraphics/ifr_2.png']

    parser = argparse.ArgumentParser(description = "Grab weather images from weather.gov and e-mail.")
    parser.add_argument("--ifr", "-i", action = "store_const", const = True, default = False,
                        help = "Only pull the ifr_1 and ifr_2 images",
                        dest = "ifr_only")
    args = parser.parse_args()
    ifr_only = args.ifr_only

    senddate = datetime.now().strftime('%m/%d/%y %H:%M')
    attachments = []

    if ifr_only:
        subject = f'Current Aviation Weather Images - midday update for {senddate}'
        body = "Attached please find your requested midday update images (Flying Weather Only)"
        URLS = URLS[-2:]
    else:
        subject = f'Current Aviation Weather Images for {senddate}'
        body = "Attached please find your requested images"
        attachments.append(get_windy())

    for url in URLS:
        res = urlopen(url)
        filename = url.split('/')[-1]
        data = res.read()
        part = make_attachment(data, filename)
        attachments.append(part)

    send_message(subject, body, attachments)
